
/**
	replaceAt function for string object
	@description Replaces a character within a string at a given index
	@param index - the index in the string to replace at
	@param charcter - the character to replace with
*/
String.prototype.replaceAt=function(index, character) {
  return this.substr(0, index) + character + this.substr(index+character.length);
}


//Create the JSGA namespace
window.JSGA = {
	/**
		The JSGA namespace object contains global configurational properties and functions.
		The property values here are global and will be applied to any population/gene object
	*/

	// the size of the population that will be generated.
	populationSize: 100,
	// The length of the gene to be generated when using simple binary gene sequences
	geneLength: 28,
	// The mutation rate to mutate genes. This is out of 1000. So 5 = 0.05% chance of mutation per gene.
	mutationRate: 5,
	// If true fitness values closer to zero are considered "higher"
	reverseFitness: true,
	// Calculate fitness function. This should be overriden with a function to determen the fitness of a gene sequence
	calculateFitness: function(gene) {},
	// Sorts genes from highest to lowest
	geneSort: function(a, b) {
		if (a.fitnessScore == b.fitnessScore) return 0;
		if (a.fitnessScore > b.fitnessScore) {
			return 1;
		} else {
			return -1;
		}
	},
	// A setter function for setting gene length
	setGeneLength: function(gl){
		this.geneLength = parseInt(gl);
	}
};
/**
	Gene class
	Construction Arguments
		geneLength - the number of bits a gene will contain.


	this is slightly misslabeled as this reprensets a sequence of genes and not a specific gene
*/
JSGA.Gene = function(opts) {};
JSGA.Gene.prototype = {
	// The gene string is a string which contains the binary representation of the gene sequence
	geneString: '',
	// The fitness score for the sequence
	fitnessScore: 0,
	// Generates a random gene sequence by looping from 0 to the geneLength and concats random 0's and 1's
	generate: function() {
		for (var i = 0; i < JSGA.geneLength; i++) {
			this.geneString = this.geneString + Math.round(Math.random());
		}
		// Calculate the fitness for the newly created gene sequence
		this.calculateFitness();
	},
	// Calls the global calculate fitness function
	calculateFitness: function() {
		this.fitnessScore = JSGA.calculateFitness(this);
	},
	// Used to change the gene by passing a new geneString
	setGeneByString: function (geneString) {
		this.geneString = geneString;
	},
	// Mutate the gene sequence
	mutate: function () {
		// loop through each gene in the sequence
		for (var i = 0; i < this.geneString.length; i++) {
			// calcualte the possible chance of a gene mutatation for this gene bit
			if (Math.floor(Math.random() * 1000) < JSGA.mutationRate) {
				// Mutate this gene bit by flipping it
				this.geneString = this.geneString.replaceAt(i,(this.geneString.charAt(i) =='0') ? '1' : '0');
			}
		}
	}
};

/**
	Population Class
		Holds an array of genes representing the population and controls generation and breeding
*/
JSGA.Population = function(opts) {};
JSGA.Population.prototype = {
	/**
		 @config generation
		 Holds the generation number for reporting purposes. This value is automatically ticked
		 up when Population.breed() is called and set to 1 when Population.generatePopulation()
		 is called.
	*/
	generation: 0,
	/**
		@config lastBreedTime
		Tracks the amount of processing time for the last breed cycle.
		If Population.breed() is called with a parameter this value will track
		the execution time for the entire process.

	*/
	lastBreedTime: 0,
	/**
		@config genes
		An array of Genes representing the current generations population.
	*/
	genes: [],
	/**
		@function generatePopulation
		@returns null
		Generates a population of Gene classes randomly. This is used to generate an
		initial population.
	*/
	generatePopulation: function() {
		this.genes=[];
		for (var i = 0; i < JSGA.populationSize; i++) {
			g = new JSGA.Gene();
			g.generate();
			this.genes.push(g);
		}
		this.sortPopulation();
		this.generation=1;
	},
	/**
		@function sortPopulation
		@returns null
		Sorts the current generations population by fitnessScore using
		the sorting algorithm defined in JSGA.geneSort()
	*/
	sortPopulation: function() {
		this.clearInvalidGenes();
		this.genes = this.genes.sort(JSGA.geneSort);
		if (JSGA.reverseFitness) {
			var maxFitness = this.genes[this.genes.length-1].fitnessScore;
			this.genes.forEach(function(itm) {
				itm.fitnessScore = maxFitness - itm.fitnessScore
			});
		}
	},
	/**
		@function clearInvalidGenes
		@retunrs null
		Removes any incoherant genes from the population. This is done by removing
		any genes whose fitness score is NaN or undefined.
	*/
	clearInvalidGenes: function() {
		var buffer = [];
		this.genes.forEach(function(itm) {
			if (itm.fitnessScore === undefined) return;
			if (itm.fitnessScore == NaN) return;
			buffer.push(itm);
		});
		this.genes = buffer;
	},
	/**
		@function breed
		@returns 0 if a solution is not found and 1 if a solution was found
		@param generations (integer) - a number of generations to breed
	*/
	breed: function(generations) {
		// set the time the processing starts
		var startTime = new Date().getTime();
		// set the default for the number of generations if it is not supplied
		if (generations===undefined) generations=1;
		// loop a number of times specified by the generations parameter
		for (var g = 0; g < generations; g++){
			var newGeneration = [];

			/**
				In order to breed to genes together we need to split both ancestor genes in half
				We calucate the length of the first and second halves of the gene
			*/

			var firstHalfLength = Math.floor(JSGA.geneLength/2);
			var secondHalfLength = JSGA.geneLength-firstHalfLength;

			/**
				Loop a number of times equal to the populationSize to create a new population
				Select 2 ancestors and create a new gene from the first half of one and the second half of another
			*/

			for (var i = 0; i<100;i++) {
				// select first ancestor
				var ancestor1 = this.getRandomGene();
				// select second ancestor excluding the first to make sure we get two different gene sequences
				var ancestor2 = this.getRandomGene(ancestor1);

				//if (ancestor1 === undefined || ancestor2 === undefined) continue;

				//Create a new Gene object
				var newGene = new JSGA.Gene();
				try {
					// set the geneString two a concatination of halves from each ancestor
					newGene.setGeneByString(ancestor1.geneString.substr(0, firstHalfLength) + ancestor2.geneString.substr(firstHalfLength, secondHalfLength));
				} catch (exception) {
					console.log('problem with gene combination: ', ancestor1, ancestor2);
				}
				// mutate the new gene
				newGene.mutate();
				// Calculate the fitness for this gene
				newGene.calculateFitness();
				// Check to see if we have a solution
				if (newGene.fitnessScore==0) {
					newGeneration.push(newGene);
					this.genes=newGeneration;
					this.sortPopulation();
					var endTime = new Date().getTime();
					this.lastBreedTime = endTime - startTime;
					return 1;
				}
				newGeneration.push(newGene);
			}
			// set the new generation
			this.genes=newGeneration;
			// sort the new generation
			this.sortPopulation();
			// tick up the generation counter
			this.generation++;
		}
		var endTime = new Date().getTime();
		this.lastBreedTime = endTime - startTime;
		return 0;
	},
	/**
		@function getAverageFitness
		@returns the average fitness of the current population
		Can be used for reporting purposes
	*/
	getAverageFitness: function() {
		var sum = 0;
		this.genes.forEach(function (itm) {
			sum += itm.fitnessScore;
		});
		return Math.floor(sum/this.genes.length);
	},
	/**
		@function getPopulationFitness
		@returns the sum of all fitnessScores from all gene sequences in the population
		This is used for calculating the weight when randomly selecting genes
	*/
	getPopulationFitness: function() {
		var fitness = 0;
		this.genes.forEach(function(itm) {
			fitness += itm.fitnessScore;
		});
		return fitness;
	},
	/**
		@function getRandomGene
		@param excludeGene (Gene) - a gene to exclude
		Retrieves a random (weighted) gene from the population using Population.selectGeneByWeight().
		If an exclude gene is passed the random gene will be selected without selecting the gene to be
		excluded.

	*/
	getRandomGene: function(excludeGene) {
		var randomWeightIndex = Math.floor(Math.random() * this.getPopulationFitness());
		return this.selectGeneByWeight(randomWeightIndex, excludeGene);
	},
	/**
		@function selectGeneByWeight
		@param weightScore a random number between 0 and the maximum weight of the population
		@param excludeGene a gene to exclude when selecting
	*/
	selectGeneByWeight: function(weightScore, excludeGene) {
		var currentWeight = 0;
		var maxWeight = this.getPopulationFitness();
		for (var i=0; i<this.genes.length;i++) {
			currentWeight += this.genes[i].fitnessScore;
			if (currentWeight > weightScore && (excludeGene===undefined || excludeGene.geneString!=this.genes[i].geneString)) {
				return this.genes[i];
			}
		}
	}
};